function sendEmail() {
  var name = $("#name").val();
  var email = $("#email").val();
  var message = $("#message").val();
  $("#returnMessage").hide();
  var errorMesagge = "Fill in the fields appropriately.";
  var successMessage = "E-mail was send successfully.";

  if (!name) {
    $("#returnMessage").show();
    $("#returnMessage").text(errorMesagge);
    document.getElementById("returnMessage").className = "text-danger";
    return false;
  }

  if (!email) {
    $("#returnMessage").show();
    $("#returnMeesage").text(errorMesagge);
    document.getElementById("returnMessage").className = "text-danger";
    return false;
  }

  if (!message) {
    $("#returnMessage").show();
    $("#returnMeesage").text(errorMesagge);
    document.getElementById("returnMessage").className = "text-danger";
    return false;
  }

  $('#sendingEmail').show();
  $("#name").prop('disabled', true);
  $("#email").prop('disabled', true);
  $("#message").prop('disabled', true);

  var model = {
    name: name,
    email: email,
    message: message,
    phone: ""
  };

  $.ajax({
    crossDomain: true,
    url: "http://email.rentaj.mk/api/Mail/sendEmailBlackup",
    method: "POST",
    data: model,
    success: function (result) {
      if (result === true) {
        clearForm();
        $("#name").focus();
        $('#sendingEmail').hide();
        $("#returnMessage").show();
        document.getElementById("returnMessage").textContent = successMessage;
        document.getElementById("returnMessage").className = "text-success";
        $("#name").prop('disabled', false);
        $("#email").prop('disabled', false);
        $("#message").prop('disabled', false);
      } else {
        $('#sendingEmail').hide();
        $("#returnMessage").show();
        $("#name").prop('disabled', false);
        $("#email").prop('disabled', false);
        $("#message").prop('disabled', false);
        document.getElementById("returnMessage").textContent = "";
      }
    },
    error: function (error) {
      $('#sendingEmail').hide();
      $("#returnMessage").show();
      $("#name").prop('disabled', false);
      $("#email").prop('disabled', false);
      $("#message").prop('disabled', false);
      document.getElementById("returnMessage").className = "text-danger";
      document.getElementById("returnMessage").textContent = "Message not successfully sent, please try again.";
    }
  })
}

function clearForm() {
  $("#name").val("");
  $("#email").val("");
  $("#message").val("");
}