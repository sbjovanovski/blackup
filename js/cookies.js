function toggleGat() {
  const allCookiesAccepted = readCookie('terms_agreed')
  if (allCookiesAccepted && allCookiesAccepted === 'yes') {
    setCookie('terms_agreed', 'no')
  } else {
    setCookie('terms_agreed', 'yes')
  }
  setTimeout(function () {
    location.reload()
  }, 100)
}

function enableGoogleAnalytics() {
  window.dataLayer = window.dataLayer || [];

  function gtag() {
    dataLayer.push(arguments);
  }

  gtag('js', new Date());
  gtag('config', 'UA-18757845-4');
}

function setCookie(name, value) {
  var d = new Date();
  d.setTime(d.getTime() + (365 * 24 * 60 * 60 * 1000));
  var expires = "expires=" + d.toUTCString();
  document.cookie = name + '=' + value + ';' + expires + ';path=/';
}

//on btn click
function analyticsBlackup() {
  var checkBox = document.getElementById("allowAnalytics");
  document.getElementById('cookie_popup').style.display = "none";
  if (checkBox.checked) {
    setCookie('terms_agreed', "yes")
  } else if (!checkBox.checked) {
    setCookie('terms_agreed', "no")
  }
  setTimeout(function () {
    location.reload()
  }, 500)
}

function readCookie(name) {
  var nameEQ = name + "=";
  var ca = document.cookie.split(';');
  for (var i = 0; i < ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') c = c.substring(1, c.length);
    if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
  }
  return null
}

// on page load
function checkCookie() {
  const isMk = location.href
  const subfolder = location.href
  let description = ''
  let title = 'Овој веб сајт користи веб колачиња "Cookies"'
  if (subfolder.indexOf("/proizvodi/") !== -1 || subfolder.indexOf("/fakti/") !== -1 || subfolder.indexOf("/preporaki/") !== -1 || subfolder.indexOf("/guide/") !== -1) {
    description = 'Би сакале да користиме веб колачиња "Cookies", со цел да го подобриме Вашето корисничко\n' + '      искуство на нашиот веб сајт. Ова ни овозможува да го подобриме Вашето идно искуство на нашиот веб сајт. Детални\n' + '      информации за начинот на кој може да ги смените Вашите преференци за веб колачиња "Cookies“ може да најдете во\n' + '      нашиот документ за <a href="../politika-na-privatnost.html"> Политика за приватност </a> / <a\n' + '          href="../politika-na-privatnost.html#settings"> Политика за веб колачиња </a>'
  } else {
    description = 'Би сакале да користиме веб колачиња "Cookies", со цел да го подобриме Вашето корисничко\n' + '      искуство на нашиот веб сајт. Ова ни овозможува да го подобриме Вашето идно искуство на нашиот веб сајт. Детални\n' + '      информации за начинот на кој може да ги смените Вашите преференци за веб колачиња "Cookies“ може да најдете во\n' + '      нашиот документ за <a href="politika-na-privatnost.html"> Политика за приватност </a> / <a\n' + '          href="politika-na-privatnost.html#settings"> Политика за веб колачиња </a>'
  }

  let lblCookies = 'Опционални веб колачиња';
  const cRead = readCookie("terms_agreed");
  if (isMk.indexOf("/en/") !== -1) {
    title = "This website uses cookies";
    if (subfolder.indexOf("/products/") !== -1 || subfolder.indexOf("/facts/") !== -1 || subfolder.indexOf("/recommendation/") !== -1 || subfolder.indexOf("/guide/") !== -1) {
      description = 'We would like to use cookies to better understand your use of this website. This enables us to improve your future \n' + ' experience on our website. Detailed information about the use of cookies on this website and how you can manage or withdraw your consent \n' + ' at any time can be found in our <a href="../privacy-policy.html">  Privacy Statement </a> / <a\n' + '          href="../privacy-policy.html#settings"> COOKIE SETTINGS </a>'
    } else {
      description = 'We would like to use cookies to better understand your use of this website. This enables us to improve your future \n' + ' experience on our website. Detailed information about the use of cookies on this website and how you can manage or withdraw your consent \n' + ' at any time can be found in our <a href="privacy-policy.html">  Privacy Statement </a> / <a\n' + '          href="privacy-policy.html#settings"> COOKIE SETTINGS </a>'
    }
    lblCookies = "Optional cookies"
  }
  document.getElementById('cookie_popup').style.display = "none";
  if (cRead === null) {
    document.getElementById('cookieTitle').innerHTML = title;
    document.getElementById('cookieDescription').innerHTML = description;
    document.getElementById('lblCookies').textContent = lblCookies;
    document.getElementById('cookie_popup').style.display = "grid";
  } else if (cRead === "yes") {
    enableGoogleAnalytics()
  }
}

function showCookiesPopup() {
  const isMk = location.href
  const subfolder = location.href
  let description = ''
  const checkBox = document.getElementById("allowAnalytics");
  let title = 'Овој веб сајт користи веб колачиња "Cookies"'
  if (subfolder.indexOf("/proizvodi/") !== -1 || subfolder.indexOf("/fakti/") !== -1 || subfolder.indexOf("/preporaki/") !== -1 || subfolder.indexOf("/guide/") !== -1) {
    description = 'Би сакале да користиме веб колачиња "Cookies", со цел да го подобриме Вашето корисничко\n' + '      искуство на нашиот веб сајт. Ова ни овозможува да го подобриме Вашето идно искуство на нашиот веб сајт. Детални\n' + '      информации за начинот на кој може да ги смените Вашите преференци за веб колачиња "Cookies“ може да најдете во\n' + '      нашиот документ за <a href="../politika-na-privatnost.html"> Политика за приватност </a> / <a\n' + '          href="../politika-na-privatnost.html#settings"> Политика за веб колачиња </a>'
  } else {
    description = 'Би сакале да користиме веб колачиња "Cookies", со цел да го подобриме Вашето корисничко\n' + '      искуство на нашиот веб сајт. Ова ни овозможува да го подобриме Вашето идно искуство на нашиот веб сајт. Детални\n' + '      информации за начинот на кој може да ги смените Вашите преференци за веб колачиња "Cookies“ може да најдете во\n' + '      нашиот документ за <a href="politika-na-privatnost.html"> Политика за приватност </a> / <a\n' + '          href="politika-na-privatnost.html#settings"> Политика за веб колачиња </a>'
  }
  let lblCookies = 'Опционални веб колачиња';
  const cRead = readCookie("terms_agreed");
  if (isMk.indexOf("/en/") !== -1) {
    title = "This website uses cookies";
    if (subfolder.indexOf("/products/") !== -1 || subfolder.indexOf("/facts/") !== -1 || subfolder.indexOf("/recommendation/") !== -1 || subfolder.indexOf("/guide/") !== -1) {
      description = 'We would like to use cookies to better understand your use of this website. This enables us to improve your future \n' + ' experience on our website. Detailed information about the use of cookies on this website and how you can manage or withdraw your consent \n' + ' at any time can be found in our <a href="../privacy-policy.html">  Privacy Statement </a> / <a\n' + '          href="../privacy-policy.html#settings"> COOKIE SETTINGS </a>'
    } else {
      description = 'We would like to use cookies to better understand your use of this website. This enables us to improve your future \n' + ' experience on our website. Detailed information about the use of cookies on this website and how you can manage or withdraw your consent \n' + ' at any time can be found in our <a href="privacy-policy.html">  Privacy Statement </a> / <a\n' + '          href="privacy-policy.html#settings"> COOKIE SETTINGS </a>'
    }
    lblCookies = "Optional cookies"
  }
  document.getElementById('cookieTitle').innerHTML = title;
  document.getElementById('cookieDescription').innerHTML = description;
  document.getElementById('lblCookies').textContent = lblCookies;
  document.getElementById('cookie_popup').style.display = "grid";
  checkBox.checked = cRead === "yes";
}