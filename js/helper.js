const getLanguage = () => location.href.indexOf("/en/") !== -1 ? 'en' : 'mk'

window.onload = () => {
  const allCookiesAccepted = readCookie('terms_agreed')
  const btnGat = document.getElementById('btnGat')
  const lang = getLanguage()

  if (btnGat) {
    if (allCookiesAccepted && allCookiesAccepted === 'yes') {
      btnGat.innerHTML = lang === 'mk' ? 'Деактивирај' : 'Deactivate'
    } else {
      btnGat.innerHTML = lang === 'mk' ? 'Активирај' : 'Activate'
    }
  }
}