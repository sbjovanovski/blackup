$(function () {
    var errorMessage = "Пополнете ги полињата соодветно.";
    var successMessage = "Успешно испратено.";

    function after_form_submitted(data) {
        if (data.result === 'success') {
            clearForm();
            //reverse the response on the button
            $('button[type="button"]', $form).each(function () {
                $btn = $(this);
                label = $btn.prop('orig_label');
                if (label) {
                    $btn.prop('type', 'submit');
                    $btn.text(label);
                    $btn.prop('orig_label', '');
                }
            });

            $("#name").focus();
            $('#sendingEmail').hide();
            $("#returnMessage").show();
            document.getElementById("returnMessage").textContent = successMessage;
            document.getElementById("returnMessage").className = "text-success";

        } else {
            $('#sendingEmail').hide();
            $("#returnMessage").show();
            document.getElementById("returnMessage").className = "text-danger";
            document.getElementById("returnMessage").textContent = "Пораката не е успешно испратена, ве молиме обидете се повторно.";

            //reverse the response on the button
            $('button[type="button"]', $form).each(function () {
                $btn = $(this);
                label = $btn.prop('orig_label');
                if (label) {
                    $btn.prop('type', 'submit');
                    $btn.text(label);
                    $btn.prop('orig_label', '');
                }
            });

        }//else
    }

    $('#reused_form').submit(function (e) {
        e.preventDefault();

        $form = $(this);
        //show some response on the button
        $('button[type="submit"]', $form).each(function () {
            $btn = $(this);
            $btn.prop('type', 'button');
            $btn.prop('orig_label', $btn.text());
            $btn.text('ИСПРАЌАЊЕ...');
        });

        $('#sendingEmail').show();

        $.ajax({
            type: "POST",
            url: 'mail/handler.php',
            data: $form.serialize(),
            success: after_form_submitted,
            dataType: 'json'
        });

    });
});

function clearForm() {
    $("#name").val("");
    $("#email").val("");
    $("#message").val("");
}