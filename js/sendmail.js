function sendEmail() {
  var name = $("#name").val();
  var email = $("#email").val();
  var message = $("#message").val();
  $("#returnMessage").hide();
  var errorMessage = "Пополнете ги полињата соодветно.";
  var successMessage = "Успешно испратено.";

  if (!name) {
    $("#returnMessage").show();
    $("#returnMessage").text(errorMessage);
    document.getElementById("returnMessage").className = "text-danger";
    return false;
  }

  if (!email) {
    $("#returnMessage").show();
    $("#returnMeesage").text(errorMessage);
    document.getElementById("returnMessage").className = "text-danger";
    return false;
  }

  if (!message) {
    $("#returnMessage").show();
    $("#returnMeesage").text(errorMessage);
    document.getElementById("returnMessage").className = "text-danger";
    return false;
  }

  $('#sendingEmail').show();
  $("#name").prop('disabled', true);
  $("#email").prop('disabled', true);
  $("#message").prop('disabled', true);

  var model = {
    name: name,
    email: email,
    message: message,
    phone: ""
  };

  $.ajax({
    crossDomain: true,
    url: "http://email.rentaj.mk/api/Mail/sendEmailBlackup",
    method: "POST",
    data: model,
    success: function (result) {
      if (result === true) {
        clearForm();
        $("#name").focus();
        $('#sendingEmail').hide();
        $("#returnMessage").show();
        document.getElementById("returnMessage").textContent = successMessage;
        document.getElementById("returnMessage").className = "text-success";
        $("#name").prop('disabled', false);
        $("#email").prop('disabled', false);
        $("#message").prop('disabled', false);
      } else {
        $('#sendingEmail').hide();
        $("#returnMessage").show();
        $("#name").prop('disabled', false);
        $("#email").prop('disabled', false);
        $("#message").prop('disabled', false);
        document.getElementById("returnMessage").textContent = "";
      }
    },
    error: function (error) {
      $('#sendingEmail').hide();
      $("#returnMessage").show();
      $("#name").prop('disabled', false);
      $("#email").prop('disabled', false);
      $("#message").prop('disabled', false);
      document.getElementById("returnMessage").className = "text-danger";
      document.getElementById("returnMessage").textContent = "Пораката не е успешно испратена, ве молиме обидете се повторно.";
    }
  })
}

function clearForm() {
  $("#name").val("");
  $("#email").val("");
  $("#message").val("");
}